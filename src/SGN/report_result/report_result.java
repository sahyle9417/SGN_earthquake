package SGN.report_result;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class report_result
 */

@WebServlet("/report_result")
public class report_result extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public report_result() {
        super();
        // TODO Auto-generated constructor stub	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//입력받은 시간과 위치에 해당하는 row들을 DB에서 불러온 상태에서 작업 시작
		
		double q1_numerator = 100;
		double q2_numerator = 100;
		double q3_numerator = 100;
		double q4_numerator = 100;
		double q5_numerator = 100;
		double q6_numerator = 100;
		double q7_numerator = 100;
		double q8_numerator = 100;
		//sum of values of answer in public's report form
		
		double q1_denominator = 50;
		double q2_denominator = 50;
		double q3_denominator = 50;
		double q4_denominator = 50;
		double q5_denominator = 50;
		double q6_denominator = 50;
		double q7_denominator = 50;
		double q8_denominator = 50;
		//number of people who doesn't answer 'not specified' for each question. 
		
		double q1_new = Double.parseDouble(request.getParameter("feel_radio"));
		q1_denominator+=1;
			if(!request.getParameter("others_feel_radio").equals("99")) {
				double others_feel = Double.parseDouble(request.getParameter("others_feel_radio"));
				q1_new = q1_new * others_feel;
			}
		q1_numerator+=q1_new;

		if(!request.getParameter("describe_radio").equals("99")) {
			double q2_new = Double.parseDouble(request.getParameter("describe_radio"));
			q2_denominator+=1;
			q2_numerator+=q2_new;
		}
		
		if(!request.getParameter("react_radio").equals("99")) {
			double q3_new = Double.parseDouble(request.getParameter("react_radio"));
			q3_denominator+=1;
			q3_numerator+=q3_new;
		}
		
		if(!request.getParameter("difficult_radio").equals("99")) {
			double q4_new = Double.parseDouble(request.getParameter("difficult_radio"));
			q4_denominator+=1;
			q4_numerator+=q4_new;
		}
		
		if(!request.getParameter("objects_radio").equals("99")) {
			double q5_new = Double.parseDouble(request.getParameter("objects_radio"));
			q5_denominator+=1;
			q5_numerator+=q5_new;
		}
		
		if(!request.getParameter("picture_radio").equals("99")) {
			double q6_new = Double.parseDouble(request.getParameter("picture_radio"));
			q6_denominator+=1;
			q6_numerator+=q6_new;
		}
	
		if(!request.getParameter("furniture_radio").equals("99")) {
			double q7_new = Double.parseDouble(request.getParameter("furniture_radio"));
			q7_denominator+=1;
			q7_numerator+=q7_new;
		}

		String building_values[] = request.getParameterValues("building_checkbox");
			if(building_values != null) {
			double q8_new = Double.parseDouble(building_values[building_values.length-1]);
			q8_denominator+=1;
			q8_numerator+=q8_new;
		}
			
		


		System.out.println(q1_numerator/q1_denominator);
		System.out.println(q2_numerator/q2_denominator);
		System.out.println(q3_numerator/q3_denominator);
		System.out.println(q4_numerator/q4_denominator);
		System.out.println(q5_numerator/q5_denominator);
		System.out.println(q6_numerator/q6_denominator);
		System.out.println(q7_numerator/q7_denominator);
		System.out.println(q8_numerator/q8_denominator);
		
		



		
/*		System.out.println("나누기 전 cws : ");
		System.out.println(cws);
		System.out.println("최종 cws에 포함시킬 질문지 개수 : ");
		System.out.println(num_of_answered);
		
		cws = cws/num_of_answered;
		System.out.println("나눈 후 cws : ");
		System.out.println(cws);

		double original_cdi = 3.40 * Math.log(cws) - 4.38;
		System.out.println("original cdi : ");
		System.out.println(original_cdi);
		
		double rounded_cdi = Math.round(original_cdi*10/10.0)+1;
		System.out.println("rounded cdi : ");
		System.out.println(rounded_cdi);*/
		
		System.out.println("------------------");
		String a = request.getParameter("lat22");
		System.out.println(a);
		System.out.println(request.getParameter("lng22"));
		System.out.println("------------------");

		double lat = Double.parseDouble(request.getParameter("lat22"));
		double lng = Double.parseDouble(request.getParameter("lng22"));
		request.setAttribute("lat", lat);
		request.setAttribute("lng", lng);
//		request.setAttribute("rounded_cdi", rounded_cdi);
		
		RequestDispatcher view = request.getRequestDispatcher("result.jsp"); // request를 내용을 result로 넘김
		System.out.println("dispatcher 실행");
		view.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}
	
	protected void calculate_cdi() throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}
	
}
