<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.sql.*,SGN.report_result.*, java.util.*, java.text.*"
    pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<jsp:useBean id="db_bean" class="SGN.report_result.DB_beans"/>
<%

// in this try catch block, we check whether there is get parameter or not. so if there is get parameter, use it to filter. but
// but if there isn't get parameter, filter data by now time.
	ArrayList<IndexData> rtn = new ArrayList<IndexData>();
	int month;
	int year;
	String get_month = request.getParameter("month");
	try{
		switch (get_month){
			case "Ene" : month = 1;
			break;
			case "Feb" : month = 2;
			break;
			case "Mar" : month = 3;
			break;
			case "Abr" : month = 4;
			break;
			case "May" : month = 5;
			break;
			case "Jun" : month = 6;
			break;
			case "Jul" : month = 7;
			break;
			case "Ago" : month = 8;
			break;
			case "Sep" : month = 9;
			break;
			case "Oct" : month = 10;
			break;
			case "Nov" : month = 11;
			break;
			case "Dic" : month = 12;
			break;	
			default : java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM");
					  String string_month = formatter.format(new java.util.Date());
					  month = Integer.parseInt(string_month);
					  break;
		}
	}
	catch(NumberFormatException | NullPointerException e){
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM");
		String string_month = formatter.format(new java.util.Date());
		
		month = Integer.parseInt(string_month);
		switch(month){
		case 1: get_month="Ene";
		break;
		case 2 : get_month = "Feb";
		break;
		case 3 : get_month = "Mar";
		break;
		case 4 : get_month = "Abr";
		break;
		case 5 : get_month = "May";
		break;
		case 6 : get_month = "Jun";
		break;
		case 7 : get_month = "Jul";
		break;
		case 8 : get_month = "Ago";
		break;
		case 9 : get_month = "Sep";
		break;
		case 10 : get_month = "Oct";
		break;
		case 11 : get_month = "Nov";
		break;
		case 12 : get_month = "Dic";
		break;
		}
		
	}

	try{
		String get_year = request.getParameter("year");
		year = Integer.parseInt(get_year);
	}
	catch(NumberFormatException e){
		java.text.SimpleDateFormat formatter2 = new java.text.SimpleDateFormat("yyyy");
		String string_year = formatter2.format(new java.util.Date());
		
		year = Integer.parseInt(string_year);
		}
	
	// getIndexDBwithFilter is bean for get data of index table that is filtered by month and year.
	rtn = db_bean.getIndexDBwithFilter(month,year);
			
	double init_mylat;
	double init_mylng;
			
	try{
		init_mylat = rtn.get(1).getIndex_lat();
		init_mylng = rtn.get(1).getIndex_lng();
	}
	catch(Exception e){
		init_mylat = 18.87;
		init_mylng = -70.19;
		}
%>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABCUMBRfwwjAdfqWEQoT3ItITOAslxfec&language=es"></script>
<script src="./static/js/heatmap.js"></script>
<script src="./static/js/gmaps-heatmap.js"></script>
	<section class="rastro_navegacion">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ul class="breadcrumb">
						<li class="active">Está aquí: &nbsp;</li>
						<li>
							<a href="http://localhost:8080/SGN_earthquake/main.jsp" class="pathway">Inicio</a>
						</li>
						<li class="active">
							<span>Mapa de Intensidad de Terremoto</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div id='number_of_dataset' style="width:100%; height:25px;" align="center"></div>
	<div style="clear:both;"></div>
	<div class='container'>
		<div id="map_row">
			<div id='left-box'>		
				<ul>
					<li><a href="map.jsp#q1">Question1. what ? </a></li>
					<li><a href="map.jsp#q2">Question2. what ? </a></li>
					<li><a href="map.jsp#q3">Question3. what ? </a></li>
					<li><a href="map.jsp#q4">Question4. what ? </a></li>
					<li><a href="map.jsp#q5">Question5. what ? </a></li>
					<li><a href="map.jsp#q6">Question6. what ? </a></li>
					<li><a href="map.jsp#q7">Question7. what ? </a></li>
					<li><a href="map.jsp#q8">Question8. what ? </a></li>
					<li><a href="map.jsp#q9">Question9. what ? </a></li>
					<li><a href="map.jsp#q10">Question10. what ? </a></li>			
				</ul>
			</div>
			<div id='middle-box'>
				<div id="map" style="width:100%; height:100%;" style="float:left;"></div>
				<div style= "margin-top: 2%; height: 62px; width: 100%; border: 1px solid black; text-align: center; box-sizing: border-box;">
					<div style="height: 20px; border: 1px solid black;box-sizing: border-box;">
						<div style="border-right: 1px solid black; width:12%; height:100%; float:left; font-weight: bold;">SACUDIDA</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left; font-stretch: ultra-condensed;">No sentía</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Débiles</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Ligero</div>
					    <div style="border-right: 1px solid black; width:10%; height:100%; float:left;">Moderar</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Fuerte</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left; font-stretch: ultra-condensed;">Muy fuerte</div>
					    <div style="border-right: 1px solid black; width:14%; height:100%; float:left;">Grave</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Violento</div>
					    <div style="width:10%; height:100%; float:left;">Extremo</div>
					</div>
					<div style="height: 20px; border: 1px solid black;box-sizing: border-box;">
						<div style="border-right: 1px solid black; width:12%; height:100%; float:left; font-weight: bold;">DAÑO</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">ninguna</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">ninguna</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">ninguna</div>
					    <div style="border-right: 1px solid black; width:10%; height:100%; float:left; font-stretch: ultra-condensed;">Muy ligero</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Ligero</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Moderar</div>
					    <div style="border-right: 1px solid black; width:14%; height:100%; float:left; font-stretch: ultra-condensed;">Moderar/Pesado</div>
					    <div style="border-right: 1px solid black; width:9%; height:100%; float:left;">Pesado</div>
					    <div style="width:10%; height:100%; float:left; font-stretch: ultra-condensed;">Muy Pesado</div>
					</div>
					<div style="height: 20px; border: 1px solid black;box-sizing: border-box;font-weight: bold;">
						<div style="border-right: 1px solid black; width:12%; height:100%; float:left;">INTENSITY</div>
					    <div style="border-right: 1px solid black; background: rgb(255, 255, 255); width:9%; height:100%; float:left;">I</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(191, 204, 255), rgb(128, 255, 255)); width:9%; height:100%; float:left;">II-III</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(128, 255, 255), rgb(122, 255, 147)); width:9%; height:100%; float:left;">IV</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(122, 255, 147), rgb(255, 255, 0)); width:10%; height:100%; float:left;">V</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(255, 255, 0), rgb(255, 200, 0)); width:9%; height:100%; float:left;">VI</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(255, 200, 0), rgb(255, 145, 0)); width:9%; height:100%; float:left;">VII</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(255, 145, 0), rgb(255, 0, 0)); width:14%; height:100%; float:left;">VIII</div>
					    <div style="border-right: 1px solid black; background: linear-gradient(to right, rgb(255, 0, 0), rgb(150, 0, 0)); width:9%; height:100%; float:left;">IX</div>
					    <div style="background: rgb(150, 0, 0); width:10%; height:100%; float:left;">X+</div>
					</div>

				</div>
			</div>
			<div id='right-box'>
				<div id='calenderwrapper'>
         			<div class="input-group">
         				<input type="text" class="form-control docs-date" name="date" placeholder="Pick a date" id="calender" style="display:none;">
	              		<div style="height:30px;">
	              			<div id="filtro" style="font-weight:bold; width:45%;color:black; float:left; font-size:16px; line-height:38px; padding-right:10%;" align="right">Filtro</div>
		              		<div style="width:55%; float:left;" align="left"><button class="btn btn-default" onclick="apply_filter()"><span style="font-size: 16px; font-weight:bold;">Aplicar</span></button></div>
	              		</div>
	              		<div style="clear:both"></div>
              			<div id="info_filter" style="font-size:14px;float:none;" align="center"></div>
              			<div id="dp_container" style="margin-bottom:15%; margin-top:3%; margin-left:auto; margin-right:auto;"></div>
	              		<div id="closebutton" style="text-align:center;">close</div>
	              		<div id="recent_event">	              		
		              		<% 
		              		ArrayList<RecentData> recentreport = db_bean.GetRecentReport();
		              		int j;
		              		%>
		              		<b><span style="font-size: 16px; font-family: Helvetica Neue; font-weight: bold;">Eventos Recien Registrados</span></b>
		              		<table class="table table-condensed table-hover" style="font-size: 14px; width:100%;height:100%; ">
		              			<tr align="center">
		              				<td> Fecha </td>
		              				<td> Hora </td>
		              				<td> CDI </td>
		              			</tr>
		              			<% for(j=0;j<recentreport.size();j++){ %>
								<tr align="center" style="font-size:14px; background: white; border-style: none;" 
								onclick="recent_popup<% out.print(j); %>(<% out.print(recentreport.get(j).getLat()); %>,
								<% out.print(recentreport.get(j).getLng()); %>,<% out.print(recentreport.get(j).getCdi()); %>
								)">
									<td style="border-style: none;"><% out.print(new SimpleDateFormat("MM/dd/yyyy").format(recentreport.get(j).getReport_datetime())); %></td>
									<td style="border-style: none;"><% out.print(recentreport.get(j).getReport_datetime().getHours());%></td>
									<td style="border-style: none;"a><% out.print(Math.round(recentreport.get(j).getCdi()*10)/10.0); %></td>
								</tr>								
			              		<%
			              		}
			              		%>
		              		</table>
	              		</div>
        	 	 	</div>
       			</div>
       		</div>
		</div> 
		<div id="mobbutton">
			<button class="btn btn-default">Seleccionar mes</button>
		</div>
		<div id="questionbox" style="padding-top:15%;">
			<a id="q1"></a>Answer1. description.Answer1. description.<br><br><br>
			<a id="q2"></a>Answer2. description.Answer2. description.<br><br><br>
			<a id="q3"></a>Answer3. description.Answer3. description.<br><br><br>
			<a id="q4"></a>Answer4. description.Answer4. description.<br><br><br>
			<a id="q5"></a>Answer5. description.Answer5. description.<br><br><br>
			<a id="q6"></a>Answer6. description.Answer6. description.<br><br><br>
			<a id="q7"></a>Answer7. description.Answer7. description.<br><br><br>
			<a id="q8"></a>Answer8. description.Answer8. description.<br><br><br>
			<a id="q9"></a>Answer9. description.Answer9. description.<br><br><br>
			<a id="q10"></a>Answer10. description.Answer10. description.
			<p id="back-top">
				<a href="map.jsp#top"><img src="./static/image/top.png?ver=1" title="top" width=40px></a>
			</p>
		</div>
	</div>
	<script>
	
		var selected_year;
    	var myLatlng = new google.maps.LatLng(<% out.print(init_mylat); %>, <% out.print(init_mylng); %>);
    	var myOptions = {
    			  zoom: 11,
    			  center: myLatlng
    			};
    	map = new google.maps.Map(document.getElementById("map"), myOptions);
    	heatmap = new HeatmapOverlay(map, 
    			{
		    // radius should be small ONLY if scaleRadius is true (or small radius is intended)
		    "radius": 0.017,
		    "maxOpacity": 0.7, 
		    // scales the radius based on map zoom
		    "scaleRadius": true, 
		    // if set to false the heatmap uses the global maximum for colorization
		    // if activated: uses the data maximum within the current map boundaries 
		    //   (there will always be a red spot with useLocalExtremas true)
		    "useLocalExtrema": false,
		    // which field name in your data represents the latitude - default "lat"
		    latField: 'lat',
		    // which field name in your data represents the longitude - default "lng"
		    lngField: 'lng',
		    // which field name in your data represents the data value - default "value"
		    valueField: 'count',
		    blur: '.9999999',
		    gradient: {
		        // enter n keys between 0 and 1 here
		        // for gradient color customization
		    	'.1': 'rgb(255, 255, 255)',
		        '.2': 'rgb(191, 204, 255)',
		        '.29': 'rgb(191, 204, 255)',
		        '.3': 'rgb(160, 230, 255)',
		        '.39': 'rgb(160, 230, 255)',
		        '.4': 'rgb(128, 255, 255)',
		        '.49': 'rgb(128, 255, 255)',
		        '.5': 'rgb(122, 255, 147)',
		        '.59': 'rgb(122, 255, 147)',
		        '.6': 'rgb(255, 255, 0)',
		        '.69': 'rgb(255, 255, 0)',
		        '.7': 'rgb(255, 200, 0)',
		        '.79': 'rgb(255, 200, 0)',
		        '.8': 'rgb(255, 145, 0)',
		        '.89': 'rgb(255, 145, 0)',
		        '.9': 'rgb(255, 0, 0)',
		        '.99': 'rgb(255, 0, 0)',
			    '1': 'rgb(150, 0, 0)',		
		      }
		  }
    			);
    	
    	// we limit zoom range because when user manipulates zoom too close or too far, heat map layer is corrupted.
    	map.setOptions({ minZoom: 7, maxZoom: 16 });
    	
    	var test1="<%out.print(rtn.size());%>";
    	var i=0;
    	var arr_test = new Array(test1)
    	arr_test =[
    	<% 
    		int i=0;
    		for(i=0;i<rtn.size();i++){ %> 
    			{lat:<% out.print(rtn.get(i).getIndex_lat()); %>,
    			 lng:<% out.print(rtn.get(i).getIndex_lng()); %>,
    			 count:<% out.print(Math.round(rtn.get(i).getIndex_cdi()*10)/10.0); %>
    			},
    			<% } %>
    			]
    	var Data = {
    			  max: 8,
    			  data: arr_test
    			};
    	
    	heatmap.setData(Data);
    	
    	$(document).ready(function(){
    	    $("button").click(function(){
    	        $("#right-box").fadeIn();
    	    });
    	});
    	
    	$(document).ready(function(){
    	    $("#closebutton").click(function(){
    	        $("#right-box").fadeOut();
    	    });
    	});
    	
    	//calender option
			$('#calender').datepicker({
				startView: 1, //show month first. 0 - day, 1 - month, 2 - year
				format : "MM-YYYY",
				inline: true,
				container: '#dp_container',
			    monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
		   		//startDate: '08-2017', // The start view date
		    	//endDate: 'today', // The end view date
		    });
			$.fn.datepicker.languages['es-ES'] = {
				    format: 'dd/mm/yyyy',
				    days: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
				    daysShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
				    daysMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
				    weekStart: 1,
				    months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
				    monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
			};
	    	var filter_month;
	    	var filter_year;

			$('#calender').on('pick.datepicker', function (e) {
				if (e.view == "month") {
					filter_month = $('.datepicker-panel').next().children().first().next().children(".picked").html();
					filter_year = $('#calender').datepicker('getDate').getFullYear();
				  }
				});
			
			function apply_filter(){
				location.href='map.jsp?month='+filter_month+"&year="+filter_year;
			}

			function recent_popup0(lat,lng,cdi){
				
				 cw=screen.availWidth;     // displayed window width
				 ch=screen.availHeight;    // displayed window height

				 ml=(cw-700)/2;    	    // location of popup x
				 mt=(ch-500)/2;         // location of popup y

				 
				var popUrl = "popup/recent_popup0.jsp?lat="+lat+"&lng="+lng+"&cdi="+cdi;	//팝업창에 출력될 페이지 URL
				var popOption = "width=700, height=500, top="+mt+",left="+ml+", resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
			}
			
			function recent_popup1(lat,lng,cdi){
				
				 cw=screen.availWidth;     // displayed window width
				 ch=screen.availHeight;    // displayed window height

				 ml=(cw-700)/2;
				 mt=(ch-500)/2;

				var popUrl = "popup/recent_popup1.jsp?lat="+lat+"&lng="+lng+"&cdi="+cdi;	//팝업창에 출력될 페이지 URL
				var popOption = "width=700, height=500, top="+mt+",left="+ml+", resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
			}

			function recent_popup2(lat,lng,cdi){
				
				 cw=screen.availWidth;     // displayed window width
				 ch=screen.availHeight;    // displayed window height

				 ml=(cw-700)/2;
				 mt=(ch-500)/2;

				var popUrl = "popup/recent_popup2.jsp?lat="+lat+"&lng="+lng+"&cdi="+cdi;	//팝업창에 출력될 페이지 URL
				var popOption = "width=700, height=500, top="+mt+",left="+ml+", resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
			}

			function recent_popup3(lat,lng,cdi){
				
				 cw=screen.availWidth;     // displayed window width
				 ch=screen.availHeight;    // displayed window height

				 ml=(cw-700)/2;        // location of popup x
				 mt=(ch-500)/2;         // location of popup y

				var popUrl = "popup/recent_popup3.jsp?lat="+lat+"&lng="+lng+"&cdi="+cdi;	//팝업창에 출력될 페이지 URL
				var popOption = "width=700, height=500, top="+mt+",left="+ml+", resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
			}

			function recent_popup4(lat,lng,cdi){
				
				 cw=screen.availWidth;     // displayed window width
				 ch=screen.availHeight;    // displayed window height

				 ml=(cw-700)/2;        // location of popup x
				 mt=(ch-500)/2;         // location of popup y

				var popUrl = "popup/recent_popup4.jsp?lat="+lat+"&lng="+lng+"&cdi="+cdi;	//팝업창에 출력될 페이지 URL
				var popOption = "width=700, height=500, top="+mt+",left="+ml+", resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
			}
			
			window.onload = function what(){
				var number_dataset = Data.data.length;
				
				$('.datepicker-panel').next().children().first().next().children().eq(<% out.print(month); %>-1).css('font-weight','bold').css('background-color','#e6f2ff');
				$('#info_filter').html("<%out.print(get_month);%>  /  <%out.print(year);%>");
				$('#number_of_dataset').html("Mapa generado a partir "+number_dataset+" reportes ciudadanos.");
				$('.datepicker-panel').next().children().first().next().children('.highlighted').attr('class','')
				};
    </script>
    
<%@ include file="footer.jsp" %>