<!DOCTYPE html>
<html>
<!-- saved from url=(0018)http://sgn.gob.do/ -->

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<% response.setContentType("text/html; charset=UTF-8"); %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-es" lang="es-es" dir="ltr">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>Enter Location</title>
		<link rel="stylesheet" href="../static/css/bootstrap.css" type="text/css">
		<link rel="stylesheet" href="../static/css/template.css" type="text/css">
		<link rel="stylesheet" href="../static/css/slide.css" type="text/css">
		<link rel="stylesheet" href="../static/css/map_list.css" type="text/css">
		<link rel="stylesheet" href="../static/css/back-top.css?ver=12" type="text/css">
		
		<link rel="icon" type="image/png" href="../static/image/pie.jpg">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="../static/js/jQuery.js"></script>
		<script src="../static/js/bootstrap.js"></script>
		<script type="text/javascript" charset="UTF-8" src="../static/js/report_form.js"></script>
		<script type="text/javascript" charset="UTF-8" src="../static/js/child_window_location.js"></script>
		<script type="text/javascript" charset="UTF-8" src="../static/js/back-top.js?ver=2"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABCUMBRfwwjAdfqWEQoT3ItITOAslxfec&language=es"></script>
		<script src="../static/js/heatmap.js"></script>
		<script src="../static/js/gmaps-heatmap.js"></script>
			<script type="text/javascript">

			function set_parent_location(){
				opener.document.getElementById("parent_latitude").value = document.getElementById("child_latitude").value;
				opener.document.getElementById("parent_longitude").value = document.getElementById("child_longitude").value;
				opener.document.getElementById("parent_latitude_show").value = document.getElementById("child_latitude").value;
				opener.document.getElementById("parent_longitude_show").value = document.getElementById("child_longitude").value;
				window.close();
			}
	   	</script>
	</head>

<body>
<div class="container" style="padding-left:40px;padding-right:40px;">
    <br>
    <b><font size="5" color="gray">Indicar Ubicación</font></b>
    <br><br>
<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#locate_by_map" role="tab" id="locate_by_map-tab" data-toggle="tab" aria-controls="locate_by_map" aria-expanded="true">Ubicación_por_Dirección</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div role="tabpanel" class="tab-pane fade active in" id="locate_by_map" aria-labelledby="locate_by_map-tab">
      
<!-- ###################### tab content start ######################### -->
    <div id="map" style="height:500px;width:500px"></div>
        <form class="form-inline">
			<div class="form-group">
				<input class="form-control" id="address" type="text" style="width:250px;" placeholder="santodomingo,reqpublic dominica">
    			<input class="form-control" id="submit" type="button" value="Geocode"><br>
				<label>latitud : </label><input id="child_latitude" type="text" class="form-control"><br>
				<label>longitud : </label><input id="child_longitude" type="text" class="form-control"><br>
			</div>
			<br><input type="button"  class="btn btn-default" value="Aplicar Ubicación" onclick="set_parent_location()">
		</form>
    <script>
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: {lat: 19.02577, lng:  -70.147705}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
            document.getElementById("child_latitude").value=results[0].geometry.location.lat()
			document.getElementById("child_longitude").value=results[0].geometry.location.lng()
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>
<!-- ##################### end ########################## -->
      </div>
    </div>
  </div>
  </div>
</body>
</html>