<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	

<%@ include file="header.jsp" %>
<!--  #################### main page content start ################### -->	

		<div class="custom2 est hidden-xs">
			<div class="container">
				<div class="row" style="padding-top:20px; padding-bottom:20px;'">
					<div class="col-sm-12">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox" style=" height:60%;">
							<div class="item active">
						    	<img src="./static/image/charlas_2_cfbddd5f546dd3865d7bcebe9de7dd7d.jpg" style="width:100%;">
						    </div>
						    <div class="item">
						      <img src="./static/image/firma_sgn_inapa_3_867299edc77f98d6039384aa2518223a.png" style="width:100%;">
						    </div>
						</div>
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		

			
			
		
<!-- mobile --> 
<div class="mob_container">
				<div class="row" style="padding-top:5px; padding-bottom:10px; height: 100%">
					<div class="col-sm-12">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="height: 100%">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox" style=" height:100%;">
							<div class="item active">
						    	<img src="./static/image/charlas_2_cfbddd5f546dd3865d7bcebe9de7dd7d.jpg" style="height:100%;">
						    </div>
						    <div class="item">
						      <img src="./static/image/firma_sgn_inapa_3_867299edc77f98d6039384aa2518223a.png" style="height:100%;">
						    </div>
						</div>
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>

</div>
<div id="navi">
      <div id="menu3">
        <h2><a href="mobinfo.jsp">INFORMACIONES</a></h2>
      </div>
      <div id="menu1">
        <h2><a href="report.jsp">REPORTE</a></h2>
      </div>
      <div id="menu2">
        <h2><a href="map.jsp">MAPA DE INTENSIDAD DE TERREMOTO</a></h2>
      </div>
</div>

		
<!--  #################### main page content end ################### -->
<%@ include file="footer.jsp" %>