 <%@ include file="admin_header.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<TITLE>Report Table</TITLE>
</HEAD>

<BODY>
	<br>
	<H2>&nbsp; Report Table</H2>
	<br>
	&nbsp; &nbsp; <button class="btn btn-primary btn-lg" onclick='redirect_to_admin_main()'><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Go Back to Admin Mainpage</button>
	<br><br>
		<div class="col-xs-8 col-sm-8 col-md-6 col-lg-6">
			<form name="admin_report_form">
				<table class="table">
					<tr>
						<td colspan="3" align="center"><div style="font-weight: bold;"><H4>Filter by Report Submit Time</H4></div></td>
					</tr>
					<tr>
						<td>From</td>
						<td>
							<div class="docs-datepicker">
								<div class="input-group">
									<input type="text" class="form-control docs-date" name="from_date" placeholder="Pick a date" id="from_date">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default docs-datepicker-trigger" name="trigger" id="trigger_btn_from_date">
											<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</div>
						</td>
						<td>
							<select class="form-control" style="width:225px" name="from_time" id="from_time">
								<option value="00:00:00">12:00 am</option>
								<option value="01:00:00"> 1:00 am</option>
								<option value="02:00:00"> 2:00 am</option>
								<option value="03:00:00"> 3:00 am</option>
								<option value="04:00:00"> 4:00 am</option>
								<option value="05:00:00"> 5:00 am</option>
								<option value="06:00:00"> 6:00 am</option>
								<option value="07:00:00"> 7:00 am</option>
								<option value="08:00:00"> 8:00 am</option>
								<option value="09:00:00"> 9:00 am</option>
								<option value="10:00:00">10:00 am</option>
								<option value="11:00:00">11:00 am</option>
								<option value="12:00:00">12:00 pm</option>
								<option value="13:00:00"> 1:00 pm</option>
								<option value="14:00:00"> 2:00 pm</option>
								<option value="15:00:00"> 3:00 pm</option>
								<option value="16:00:00"> 4:00 pm</option>
								<option value="17:00:00"> 5:00 pm</option>
								<option value="18:00:00"> 6:00 pm</option>
								<option value="19:00:00"> 7:00 pm</option>
								<option value="20:00:00"> 8:00 pm</option>
								<option value="21:00:00"> 9:00 pm</option>
								<option value="22:00:00"> 10:00 pm</option>
								<option value="23:00:00"> 11:00 pm</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>To</td>
						<td>
							<div class="docs-datepicker">
									<div class="input-group">
										<input type="text" class="form-control docs-date" name="to_date" placeholder="Pick a date" id="to_date">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default docs-datepicker-trigger" name="trigger" id="trigger_btn_to_date">
												<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
						</td>
						<td>
							<select class="form-control" style="width:225px" name="to_time" id="to_time">
								<option value="00:00:00">12:00 am</option>
								<option value="01:00:00"> 1:00 am</option>
								<option value="02:00:00"> 2:00 am</option>
								<option value="03:00:00"> 3:00 am</option>
								<option value="04:00:00"> 4:00 am</option>
								<option value="05:00:00"> 5:00 am</option>
								<option value="06:00:00"> 6:00 am</option>
								<option value="07:00:00"> 7:00 am</option>
								<option value="08:00:00"> 8:00 am</option>
								<option value="09:00:00"> 9:00 am</option>
								<option value="10:00:00">10:00 am</option>
								<option value="11:00:00">11:00 am</option>
								<option value="12:00:00">12:00 pm</option>
								<option value="13:00:00"> 1:00 pm</option>
								<option value="14:00:00"> 2:00 pm</option>
								<option value="15:00:00"> 3:00 pm</option>
								<option value="16:00:00"> 4:00 pm</option>
								<option value="17:00:00"> 5:00 pm</option>
								<option value="18:00:00"> 6:00 pm</option>
								<option value="19:00:00"> 7:00 pm</option>
								<option value="20:00:00"> 8:00 pm</option>
								<option value="21:00:00"> 9:00 pm</option>
								<option value="22:00:00"> 10:00 pm</option>
								<option value="23:00:00"> 11:00 pm</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="3" align="center">
							<!-- show_report_table() function is declared in admin_header.jsp -->
							<button class="btn btn-primary btn-lg" onclick='show_report_table(1)'>Apply Filter</button>&nbsp;&nbsp;
							<button class="btn btn-primary btn-lg" onclick='show_report_table(0)'>Show All</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	<br>
	<br>
	<TABLE class="table" style="table-layout: fixed; word-break: break-all;">
		<THEAD>
			<TR style="font-weight: bold; background: #c9dff0;">
				<TD style="width: 100px">Row ID</TD>
				<TD style="width: 100px">Latitude</TD>
				<TD style="width: 100px">Longitude</TD>
				<TD style="width: 200px">Earthquake Time</TD>
				<TD style="width: 200px">Report Submit Time</TD>
				<TD style="width: 50px">Q1</TD>
				<TD style="width: 50px">Q2</TD>
				<TD style="width: 50px">Q3</TD>
				<TD style="width: 50px">Q4</TD>
				<TD style="width: 50px">Q5</TD>
				<TD style="width: 50px">Q6</TD>
				<TD style="width: 50px">Q7</TD>
				<TD style="width: 50px">Q8</TD>
				<TD style="width: 50px">Q9</TD>
				<TD style="width: 50px">Q10</TD>
				<TD style="width: 50px">Q11</TD>
				<TD style="width: 50px">Q12</TD>
				<TD style="width: 50px">Q13</TD>
				<TD style="width: 50px">Q14</TD>
				<TD style="width: 50px">Q15</TD>
				<TD style="width: 50px">Q16</TD>
				<TD style="width: 200px">User Comments</TD>
				<TD style="width: 200px">User Name</TD>
				<TD style="width: 200px">User Email</TD>
				<TD style="width: 200px">User Phone Number</TD>
			</TR>
		</THEAD>
	</TABLE>
</BODY>
</HTML>