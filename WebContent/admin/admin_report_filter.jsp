 <%@ include file="admin_header.jsp" %>
<%
	String from_datetime_string = request.getParameter("from_date") + " " + request.getParameterValues("from_time")[0];
	java.sql.Timestamp from_datetime = java.sql.Timestamp.valueOf(from_datetime_string);
				
	String to_datetime_string = request.getParameter("to_date") + " " + request.getParameterValues("to_time")[0];
	java.sql.Timestamp to_datetime = java.sql.Timestamp.valueOf(to_datetime_string);
%>
<TITLE>Report Table Filter</TITLE>
</HEAD>

<BODY>
	<br>
 	<H2>Report Table <small>Filtered Result between <%=from_datetime%> and <%=to_datetime%></small></H2>
	<br>
	&nbsp; &nbsp; <button class="btn btn-primary btn-lg" onclick='redirect_to_admin_report_main()'><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Go back to Report Table page</button>
	<br><br>
	<HR>
	<div style="text-align:center">
	<H4>Export Rows of Report Table</label> (Submitted between <%=from_datetime%> and <%=to_datetime%>) To CSV file</H4><br>
		<form action=admin_report_csv.jsp>
			<div class="form-inline form-group">
				<H5>CSV Filepath : <input type="text" class="form-control" style="width:250px;" name="filepath"><br><br>
				CSV Filename : <input type="text" class="form-control" style="width:200px;" name="filename"> .csv</H5></H5>
			<input type="hidden" name="tablename_filter" value="report_filter">
			<input type="hidden" name="from_datetime" value="<%=from_datetime%>">
			<input type="hidden" name="to_datetime" value="<%=to_datetime%>">
		<br><button type="submit" class="btn btn-primary btn-lg">Export CSV</button>
		</div>
		</form>
	</div>
	<HR>
	<br><br>
	<TABLE class="table" style="table-layout: fixed; word-break: break-all;">
		<THEAD>
			<TR style="font-weight: bold; background: #c9dff0;">
				<TD style="width: 100px">Row ID</TD>
				<TD style="width: 100px">Latitude</TD>
				<TD style="width: 100px">Longitude</TD>
				<TD style="width: 200px">Earthquake Time</TD>
				<TD style="width: 200px">Report Submit Time</TD>
				<TD style="width: 50px">Q1</TD>
				<TD style="width: 50px">Q2</TD>
				<TD style="width: 50px">Q3</TD>
				<TD style="width: 50px">Q4</TD>
				<TD style="width: 50px">Q5</TD>
				<TD style="width: 50px">Q6</TD>
				<TD style="width: 50px">Q7</TD>
				<TD style="width: 50px">Q8</TD>
				<TD style="width: 50px">Q9</TD>
				<TD style="width: 50px">Q10</TD>
				<TD style="width: 50px">Q11</TD>
				<TD style="width: 50px">Q12</TD>
				<TD style="width: 50px">Q13</TD>
				<TD style="width: 50px">Q14</TD>
				<TD style="width: 50px">Q15</TD>
				<TD style="width: 50px">Q16</TD>
				<TD style="width: 200px">User Comments</TD>
				<TD style="width: 200px">User Name</TD>
				<TD style="width: 200px">User Email</TD>
				<TD style="width: 200px">User Phone Number</TD>
			</TR>
		</THEAD>
 		<TBODY>
			<%				
				ArrayList<ReportData> report_data_array = db_bean.getReportTable(from_datetime, to_datetime);
				for (int i = 0; i < report_data_array.size(); i++) {
			%>
			<TR>
				<TD><%=Math.round(report_data_array.get(i).getId())%></TD>
				<TD><%=report_data_array.get(i).getLat()%></TD>
				<TD><%=report_data_array.get(i).getLng()%></TD>
				<TD><%=report_data_array.get(i).getEarthquake_reported_datetime()%></TD>
				<TD><%=report_data_array.get(i).getSubmit_datetime()%></TD>
				<TD><%=report_data_array.get(i).getQ1_feel()%></TD>
				<TD><%=report_data_array.get(i).getQ2_situation_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ3_asleep_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ4_others()%></TD>
				<TD><%=report_data_array.get(i).getQ5_describe()%></TD>
				<TD><%=report_data_array.get(i).getQ6_react()%></TD>
				<TD><%=report_data_array.get(i).getQ7_respond_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ8_difficult()%></TD>
				<TD><%=report_data_array.get(i).getQ9_swing_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ10_hear_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ11_objects()%></TD>
				<TD><%=report_data_array.get(i).getQ12_picture()%></TD>
				<TD><%=report_data_array.get(i).getQ13_furniture()%></TD>
				<TD><%=report_data_array.get(i).getQ14_heavy_appliance_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ15_walls_fences_dummy()%></TD>
				<TD><%=report_data_array.get(i).getQ16_buildings()%></TD>
				<TD><span class="more"><%=report_data_array.get(i).getUser_comments()%></span></TD>
				<TD><%=report_data_array.get(i).getUser_name()%></TD>
				<TD><%=report_data_array.get(i).getUser_email()%></TD>
				<TD><%=report_data_array.get(i).getUser_phone()%></TD>
			</TR>
			<%
				}
			%>
		</TBODY>
	</TABLE>
</BODY>
</HTML>
