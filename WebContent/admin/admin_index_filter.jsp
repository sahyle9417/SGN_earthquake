 <%@ include file="admin_header.jsp" %>
<%
	String from_datetime_string = request.getParameter("from_date") + " " + request.getParameterValues("from_time")[0];
	java.sql.Timestamp from_datetime = java.sql.Timestamp.valueOf(from_datetime_string);
				
	String to_datetime_string = request.getParameter("to_date") + " " + request.getParameterValues("to_time")[0];
	java.sql.Timestamp to_datetime = java.sql.Timestamp.valueOf(to_datetime_string);
%>
<TITLE>Index Table Filter</TITLE>
</HEAD>

<BODY>
	<H2>Index Table <small>Filtered Result <%=from_datetime%> and <%=to_datetime%></small></H2>
	<br>
	&nbsp; &nbsp; <button class="btn btn-primary btn-lg" onclick='redirect_to_admin_index_main()'><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Go back to Report Table page</button>
	<br><br>
	<HR>
	<div style="text-align:center">
	<H4>Export Rows of Report Table</label> (Earthquake between <%=from_datetime%> and <%=to_datetime%>) To CSV file</H4><br>
		<form action=admin_report_csv.jsp>
			<div class="form-inline form-group">
				<H5>CSV Filepath : <input type="text" class="form-control" style="width:250px;" name="filepath"><br><br>
				CSV Filename : <input type="text" class="form-control" style="width:200px;" name="filename"> .csv</H5></H5>
			<input type="hidden" name="tablename_filter" value="index_filter">
			<input type="hidden" name="from_datetime" value="<%=from_datetime%>">
			<input type="hidden" name="to_datetime" value="<%=to_datetime%>">
		<br><button type="submit" class="btn btn-primary btn-lg">Export CSV</button>
		</div>
		</form>
	</div>
	<HR>
	<br><br>
	<TABLE class="table" style="table-layout: fixed; word-break: break-all;">
		<THEAD>
			<TR style="font-weight: bold; background: #c9dff0;">
				<TD style="width: 100px">Row ID</TD>
				<TD style="width: 100px">Latitude</TD>
				<TD style="width: 100px">Longitude</TD>
				<TD style="width: 200px">Earthquake Time</TD>
				<TD style="width: 130px">Sum of Q1*Q4</TD>
				<TD style="width: 130px">Count of Q1*Q4</TD>
				<TD style="width: 130px">Sum of Q5</TD><!-- At the report form, each question has it’s different value, more important and serious questions have a higher value. And Each answers do too. We multiply both values to get a result. After we gather results from similar area and similar time, We sum those up. Sum of Q5 has summed answered question 5 -->
				<TD style="width: 130px">Count of Q5</TD><!-- Number of people who didn't choose "No es relevante(Not specified)" as an answer. We have to maintain this value for each questions, because we have to exclude reports who answered "Not specified" for calculating CWS and CDI. Any question that is replied "No es relevante(Not specified)" is excluded from the calculation of CWS and CDI. This answer has a 0 value and should be taken away in the denominator during calculation of CWS and CDI. -->
				<TD style="width: 130px">Sum of Q6</TD><!-- Detailed information is at https://earthquake.usgs.gov/data/dyfi/background.php -->
				<TD style="width: 130px">Count of Q6</TD>
				<TD style="width: 130px">Sum of Q8</TD>
				<TD style="width: 130px">Count of Q8</TD>
				<TD style="width: 130px">Sum of Q11</TD>
				<TD style="width: 130px">Count of Q11</TD>
				<TD style="width: 130px">Sum of Q12</TD>
				<TD style="width: 130px">Count of Q12</TD>
				<TD style="width: 130px">Sum of Q13</TD>
				<TD style="width: 130px">Count of Q13</TD>
				<TD style="width: 130px">Sum of Q16</TD>
				<TD style="width: 130px">Count of Q16</TD>
				<TD style="width: 50px">CWS</TD><!-- CWS = (sum of q1*q4)/(count of q1*q4) + (sum of q5)/(count of q5) + (sum of q6)/(count of q6) + (sum of q8)/(count of q8) + (sum of q11)/(count of q11) + (sum of q12)/(count of q12) + (sum of q13)/(count of q13) + (sum of q16)/(count of q16) -->
				<TD style="width: 50px">CDI</TD><!-- CDI = 3.40 ln(CWS) - 4.38 -->
				<!-- detailed calculation process are in the admin.java -->
			</TR>
		</THEAD>
 		<TBODY>
			<%
				ArrayList<IndexData> index_data_array = db_bean.getIndexTable(from_datetime, to_datetime);
				for (int i = 0; i < index_data_array.size(); i++) {
			%>
			<TR>
				<TD><%=Math.round(index_data_array.get(i).getIndex_id())%></TD>
				<TD><%=index_data_array.get(i).getIndex_lat()%></TD>
				<TD><%=index_data_array.get(i).getIndex_lng()%></TD>
				<TD><%=index_data_array.get(i).getIndex_earthquake_reported_datetime()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_times()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_times()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q5()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q5()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q6()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q6()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q8()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q8()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q11()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q11()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q12()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q12()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q13()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q13()%></TD>
				<TD><%=index_data_array.get(i).getIndex_sum_q16()%></TD>
				<TD><%=index_data_array.get(i).getIndex_count_q16()%></TD>
				<TD><%=index_data_array.get(i).getIndex_cws()%></TD>
				<TD><%=index_data_array.get(i).getIndex_cdi()%></TD>
			</TR>
			<%
				}
			%>
		</TBODY>
	</TABLE>
</BODY>
</HTML>
