<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.sql.*" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db_bean" scope="request" class="SGN.report_result.DB_beans"/>

<%
if(db_bean.synchronize_db()){
	response.sendRedirect("/SGN_earthquake/admin/admin_index.jsp");
}
else{
	throw new Exception("Occured error!");
}
%>

